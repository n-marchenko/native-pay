﻿using System;
using System.Linq;
using NativePayTest.Core;

namespace NativePayTest.Host
{
    internal class Program
    {
        private static void Main()
        {
            ReadFromConsole(out var printCount, out var persons);

            var queue = new PersonsQueue();

            foreach (var person in persons)
            {
                queue.Enqueue(person);
            }

            PrintQueue(queue, printCount);

            queue.SortByPhoneNumber();

            PrintQueue(queue);

            queue.SortById();

            PrintQueue(queue);
        }

        private static void ReadFromConsole(out int printCount, out Person[] persons)
        {
            var index = 0;

            Console.WriteLine("Example input: 3 +77777777 +88888888 +99999999 +1000000 +3333333");
            Console.WriteLine("Please, enter a string as in example:");

            var input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            var inputParams = input.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            if (!int.TryParse(inputParams[0], out printCount))
            {
                throw new ArgumentException(nameof(printCount));
            }

            if (printCount > inputParams.Length - 1)
            {
                throw new ArgumentOutOfRangeException(nameof(printCount));
            }

            persons = inputParams
                .Skip(1)
                .Select(x => new Person
                {
                    Id = index++,
                    Phone = x
                })
                .ToArray();
        }

        private static void PrintQueue(PersonsQueue queue, int? count = null)
        {
            Console.WriteLine("".PadRight(Console.BufferWidth, '-'));
            

            var limit = count ?? queue.Count();

            for (var i = 0; i < limit; i++)
            {
                Console.WriteLine(queue[i]);
            }
        }
    }
}
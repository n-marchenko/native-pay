﻿using System.Collections;

namespace NativePayTest.Core.Comparers
{
    public class PersonIdComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((Person)x).Id.CompareTo(((Person)y).Id);
        }
    }
}

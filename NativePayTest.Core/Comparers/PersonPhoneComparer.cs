﻿using System;
using System.Collections;

namespace NativePayTest.Core.Comparers
{
    public class PersonPhoneComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return string.Compare(((Person) x).Phone, ((Person) y).Phone, StringComparison.Ordinal);
        }
    }
}
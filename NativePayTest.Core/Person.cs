﻿namespace NativePayTest.Core
{
    public class Person
    {
        public int Id { get; set; }
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"ID: {Id}, Phone: {Phone}";
        }
    }
}
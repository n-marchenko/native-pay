﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NativePayTest.Core.Abstractions
{
    internal class QueueEnumerator<T> : IEnumerator<T>
    {
        private readonly ArrayList _collection;

        private int _currentPosition = -1;

        public QueueEnumerator(ArrayList collection)
        {
            _collection = collection;
        }

        public bool MoveNext()
        {
            _currentPosition++;
            return _currentPosition < _collection.Count;
        }

        public void Reset()
        {
            _currentPosition = -1;
        }

        public T Current
        {
            get
            {
                try
                {
                    return (T) _collection[_currentPosition];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NativePayTest.Core.Abstractions
{
    public class Queue<T> : IQueue<T>, IEnumerable<T>
    {
        private readonly ArrayList _queue;

        public Queue()
        {
            _queue = new ArrayList();
        }

        public T this[int index]
        {
            get
            {
                if (index >= Size)
                {
                    throw new IndexOutOfRangeException();
                }

                return (T)_queue[index];
            }
        }

        public void Enqueue(T item)
        {
            _queue.Add(item);
        }

        public T Dequeue()
        {
            if (IsEmpty)
            {
                throw new ApplicationException("Queue is empty");
            }

            var first = _queue[0];

            _queue.RemoveAt(0);

            return (T)first;
        }

        public bool IsEmpty => _queue.Count == 0;

        public int Size => _queue.Count;

        public void Sort(IComparer comparer)
        {
            _queue.Sort(comparer);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new QueueEnumerator<T>(_queue);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;

namespace NativePayTest.Core.Abstractions
{
    public interface IQueue<T>
    {
        bool IsEmpty { get; }

        int Size { get; }

        void Enqueue(T item);

        T Dequeue();

        void Sort(IComparer comparer);
    }
}
﻿using NativePayTest.Core.Abstractions;
using NativePayTest.Core.Comparers;

namespace NativePayTest.Core
{
    public class PersonsQueue : Queue<Person>
    {
        public void SortById()
        {
            Sort(new PersonIdComparer());
        }

        public void SortByPhoneNumber()
        {
            Sort(new PersonPhoneComparer());
        }
    }
}